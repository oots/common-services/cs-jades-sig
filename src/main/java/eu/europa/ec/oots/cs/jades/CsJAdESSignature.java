/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.oots.cs.jades;

import eu.europa.esig.dss.enumerations.*;
import eu.europa.esig.dss.jades.HTTPHeaderDigest;
import eu.europa.esig.dss.jades.JAdESSignatureParameters;
import eu.europa.esig.dss.jades.signature.JAdESService;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.spi.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class CsJAdESSignature {

    private static final Logger log = LoggerFactory.getLogger(CsJAdESSignature.class);
    private SignatureValue signatureValue;
    private DSSPrivateKeyEntry privateKey;
    private DSSDocument signedDocument;
    private JAdESSignatureParameters parameters;
    private Pkcs12SignatureToken token;
    private JAdESService jAdESService;
    public static final String DOCUMENT_NAME = "oots-response-sig";
    private DigestAlgorithm DIGEST_ALGORITHM;
    private EncryptionAlgorithm ENCRYPTION_ALGORITHM;

    private final CertificateProperties certificateProperties;
    private final CommonCertificateVerifier commonCertificateVerifier;

    public CsJAdESSignature() {
        this(getCertificateProperties(), getCommonCertificateVerifier());
    }

    public CsJAdESSignature(CertificateProperties certificateProperties, CommonCertificateVerifier commonCertificateVerifier) {
        this.certificateProperties = certificateProperties;
        this.commonCertificateVerifier = commonCertificateVerifier;
        init();
    }

    /**
     * Initialization of the certificate properties, service and private key
     */
    public void init() {
        if (certificateProperties == null || certificateProperties.getFilename() == null || certificateProperties.getPassword() == null) {
            return;
        }
        // Load certificate
        InputStream inputStream = loadCertificate(certificateProperties.getFilename());

        String password = certificateProperties.getPassword();

        if(certificateProperties.getAlgorithm().equals("EdDSA")){
            ENCRYPTION_ALGORITHM = EncryptionAlgorithm.EDDSA;
            DIGEST_ALGORITHM = DigestAlgorithm.SHA512;
        } else if(certificateProperties.getAlgorithm().equals("ECDSA")){
            ENCRYPTION_ALGORITHM = EncryptionAlgorithm.ECDSA;
            DIGEST_ALGORITHM = DigestAlgorithm.SHA256;
        } else {
            throw new IllegalArgumentException("Algorithm not supported");
        }

        jAdESService = new JAdESService(commonCertificateVerifier);
        token = new Pkcs12SignatureToken(inputStream, new KeyStore.PasswordProtection(password.toCharArray()));
        List<DSSPrivateKeyEntry> keys = token.getKeys();
        for (DSSPrivateKeyEntry entry : keys) {
            privateKey = entry;
            log.debug("{}", entry.getCertificate().getCertificate().toString());
        }

        // JAdES signature params
        parameters = new JAdESSignatureParameters();
        assert privateKey != null;
        parameters.setSigningCertificate(privateKey.getCertificate());
        parameters.setCertificateChain(privateKey.getCertificateChain());
        // Set Detached packaging - STEP 1
        parameters.setSignaturePackaging(SignaturePackaging.DETACHED);
        // Set Mechanism HttpHeaders for 'sigD' header - STEP 2 and STEP 3
        parameters.setSigDMechanism(SigDMechanism.HTTP_HEADERS);
        parameters.setBase64UrlEncodedPayload(false);
        parameters.setJwsSerializationType(JWSSerializationType.COMPACT_SERIALIZATION);
        parameters.setSignatureLevel(SignatureLevel.JAdES_BASELINE_B);
        // STEP 5 select encryption and digest algorithm
        parameters.setEncryptionAlgorithm(ENCRYPTION_ALGORITHM);
        parameters.setDigestAlgorithm(DIGEST_ALGORITHM);
    }

    /**
     * Signs a json string with ETSI ESI Jades compliant signature rules on :
     * <a href="https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/3.1.4+Query+Interface+Specification+of+the+DSD#id-3.1.4QueryInterfaceSpecificationoftheDSD-4.6.ResponseSignature">...</a>
     *
     * @param json with payload
     * @return CsJades object with digest and signed document
     **/
    public CsJades buildSignedDocument(String json) throws IOException {
        CsJades csJades = new CsJades();
        DSSDocument toSignDocument = new InMemoryDocument(json.getBytes());
        toSignDocument.setName(DOCUMENT_NAME);
        // STEP 4
        HTTPHeaderDigest digest = new HTTPHeaderDigest(toSignDocument, DIGEST_ALGORITHM);
        csJades.setDigest(digest.getValue());
        List<DSSDocument> documentsToSign = new ArrayList<>();
        documentsToSign.add(digest);

        ToBeSigned toBeSigned = jAdESService.getDataToSign(documentsToSign, parameters);
        signatureValue = token.sign(toBeSigned, DIGEST_ALGORITHM, privateKey);
        signedDocument = jAdESService.signDocument(digest, parameters, signatureValue);

        csJades.setSignedDocument(new String(signedDocument.openStream().readAllBytes()));

        return csJades;
    }

    public static CertificateProperties getCertificateProperties() {
        try (InputStream input =
                     CsJAdESSignature.class.getClassLoader().getResourceAsStream("config.properties")) {
            Properties prop = new Properties();

            if (input == null) {
                log.error("Unable to find config.properties");
                return null;
            }

            prop.load(input);
            CertificateProperties properties = new CertificateProperties();
            properties.setFilename(prop.getProperty("certificate.filename"));
            properties.setPassword(prop.getProperty("certificate.password"));
            properties.setAlgorithm(prop.getProperty("certificate.algorithm"));
            return properties;
        } catch (IOException ex) {
            log.error("An error occurred while loading config.");
        }
        return null;
    }

    private InputStream loadCertificate(String filename) {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(filename);
        } catch (IOException e) {
            log.info("Could not load from file, trying from classloader: {}", e.getMessage());
            ClassLoader classLoader = getClass().getClassLoader();
            inputStream = classLoader.getResourceAsStream(filename);
        }
        return inputStream;
    }

    private static CommonCertificateVerifier getCommonCertificateVerifier() {
        return new CommonCertificateVerifier();
    }

    public SignatureValue getSignatureValue() {
        return signatureValue;
    }

    public DSSDocument getSignedDocument() {
        return signedDocument;
    }

    public DSSPrivateKeyEntry getPrivateKey() {
        return privateKey;
    }
}
