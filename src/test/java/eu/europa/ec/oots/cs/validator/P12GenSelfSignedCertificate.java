/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.oots.cs.validator;

import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.util.Base64URL;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.util.Date;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

class P12GenSelfSignedCertificate {

    public static final String EC = "EC";
    public static final String EdDSA = "EdDSA";

    //"EdDSA" for Ed25519 OR "EC" for ECDSA
    public static final String ALGORITHM = EC;
    public static final String P_12_FILE_PATH = "src/test/resources/oots-cs-dev-ecdsa.p12";
    public static final String JWK_FILE_PATH = "src/test/resources/oots-cs-dev.jwk";
    public static final String P_12_PASSWORD = "oots123";
    public static final String ALIAS = "oots-cs-test";

    public static final String SHA256withECDSA = "SHA256withECDSA";


    void deleteExistingFiles() throws IOException {
        Path pathP12 = Paths.get(P_12_FILE_PATH);
        if (Files.exists(pathP12)) {
            Files.delete(pathP12);
        }
        Path pathJwk = Paths.get(JWK_FILE_PATH);
        if (Files.exists(pathJwk)) {
            Files.delete(pathJwk);
        }
    }

    //@Test
    void genFile() throws Exception {
        deleteExistingFiles();
        // Generate key pair
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
        // used in case of ECDSA
        keyPairGenerator.initialize(new ECGenParameterSpec("secp256r1"));
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();

        // Create a self-signed certificate
        X509Certificate certificate = generateSelfSignedCertificate(keyPair);

        // Create a PKCS12 keystore and store the key pair
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(null, null);
        keyStore.setKeyEntry(ALIAS, privateKey, P_12_PASSWORD.toCharArray(),
                new Certificate[]{certificate});

        // Save the keystore to a file
        try (FileOutputStream fos = new FileOutputStream(P_12_FILE_PATH)) {
            keyStore.store(fos, P_12_PASSWORD.toCharArray());
        }
        while (!Files.exists(Paths.get(P_12_FILE_PATH)) || !isFileAvailable(P_12_FILE_PATH)) {
            Thread.sleep(100);
        }
        // Convert the key pair to JWK format
        convertToJwk();
        assertTrue(Files.exists(Paths.get(P_12_FILE_PATH)), "The .p12 file was created");
        assertTrue(Files.exists(Paths.get(JWK_FILE_PATH)), "The .jwk file was created");
    }

    private boolean isFileAvailable(String filePath) {
        try (FileInputStream fis = new FileInputStream(filePath)) {
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private static X509Certificate generateSelfSignedCertificate(KeyPair keyPair) throws Exception {
        X500Name issuer = new X500Name("CN=cs-dev, OU=FOR TESTING PURPOSES ONLY, O=OOTS");
        BigInteger serialNumber = new BigInteger(64, new SecureRandom());
        Date notBefore = new Date();
        Date notAfter = new Date(notBefore.getTime() + 365 * 86400000L); // 1 year validity

        X509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(
                issuer, serialNumber, notBefore, notAfter, issuer, keyPair.getPublic());

        Provider bcProvider = new BouncyCastleProvider();
        ContentSigner contentSigner = null;
        if(ALGORITHM.equals("EdDSA")) {
                contentSigner =  new JcaContentSignerBuilder(EdDSA)
                        .setProvider(bcProvider).build(keyPair.getPrivate());
        }else if(ALGORITHM.equals("EC")) {
            contentSigner = new JcaContentSignerBuilder(SHA256withECDSA)
                    .setProvider(bcProvider).build(keyPair.getPrivate());
        }

        return new JcaX509CertificateConverter().setProvider(bcProvider)
                .getCertificate(certBuilder.build(contentSigner));
    }

    void convertToJwk() throws IOException, KeyStoreException, CertificateException,
            NoSuchAlgorithmException,  UnrecoverableKeyException {

        // Load the .p12 keystore
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        try (InputStream inputStream = new FileInputStream(P_12_FILE_PATH)) {
            keyStore.load(inputStream, P_12_PASSWORD.toCharArray());
        }

        // Get the private key and certificate
        PrivateKey privateKey = (PrivateKey) keyStore.getKey(ALIAS, P_12_PASSWORD.toCharArray());
        Certificate certificate = keyStore.getCertificate(ALIAS);
        PublicKey publicKey = certificate.getPublicKey();

        JWK jwk;
        if (publicKey instanceof ECPublicKey) {
            jwk = new ECKey.Builder(Curve.P_256, (ECPublicKey) publicKey)
                    .privateKey(privateKey)
                    .keyID(ALIAS)
                    .algorithm(new Algorithm("ES256"))
                    .keyUse(KeyUse.SIGNATURE)
                    .keyOperations(Set.of(KeyOperation.VERIFY, KeyOperation.SIGN))
                    .build();
        } else if (publicKey.getAlgorithm().equals("EdDSA")) {
            Base64URL x = Base64URL.encode(publicKey.getEncoded());
            Base64URL d = Base64URL.encode(privateKey.getEncoded());
            jwk = new OctetKeyPair.Builder(Curve.Ed25519, x)
                    .d(d)
                    .keyID(ALIAS)
                    .algorithm(new Algorithm("EdDSA"))
                    .keyUse(KeyUse.SIGNATURE)
                    .keyOperations(Set.of(KeyOperation.VERIFY, KeyOperation.SIGN))
                    .build();
        } else {
            throw new IllegalArgumentException("Unsupported public key algorithm: " + publicKey.getAlgorithm());
        }

        // Write the JWK to a file
        try (FileWriter fileWriter = new FileWriter(JWK_FILE_PATH)) {
            fileWriter.write(jwk.toJSONString());
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

}
