/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.oots.cs.jws;

import eu.europa.ec.oots.cs.jades.CertificateProperties;
import eu.europa.ec.oots.cs.jades.CsJAdESSignature;
import eu.europa.ec.oots.cs.jades.CsJades;
import eu.europa.esig.dss.detailedreport.jaxb.XmlSemantic;
import eu.europa.esig.dss.enumerations.TokenExtractionStrategy;
import eu.europa.esig.dss.enumerations.ValidationLevel;
import eu.europa.esig.dss.jades.HTTPHeader;
import eu.europa.esig.dss.jades.validation.JWSCompactDocumentValidator;
import eu.europa.esig.dss.jaxb.object.Message;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.spi.policy.SignaturePolicyProvider;
import eu.europa.esig.dss.spi.validation.CertificateVerifier;
import eu.europa.esig.dss.spi.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.spi.x509.CertificateSource;
import eu.europa.esig.dss.spi.x509.CommonTrustedCertificateSource;
import eu.europa.esig.dss.spi.x509.KeyStoreCertificateSource;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import eu.europa.esig.dss.validation.executor.signature.DefaultSignatureProcessExecutor;
import eu.europa.esig.dss.validation.identifier.UserFriendlyIdentifierProvider;
import eu.europa.esig.dss.validation.reports.Reports;
import org.junit.jupiter.api.Test;


import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class CsJAdESSignatureTest {

    @Test
    void testBuildSignatureJson() throws Exception {
        CsJAdESSignature csJAdESSignature = new CsJAdESSignature();
        String testJson = "{ \"test\" : 1234 }";
        CsJades result = csJAdESSignature.buildSignedDocument(testJson);

        JWSCompactDocumentValidator validator = new JWSCompactDocumentValidator(csJAdESSignature.getSignedDocument());
        CommonCertificateVerifier certificateVerifier = new CommonCertificateVerifier(true);
        validator.setCertificateVerifier(certificateVerifier);
        assertTrue(validator.isSupported(csJAdESSignature.getSignedDocument()));

        assertNotNull(result);
    }

    @Test
    void validationTest() throws IOException {
        CertificateProperties certificateProperties = CsJAdESSignature.getCertificateProperties();

        String signature = "eyJhbGciOiJFUzI1NiIsImtpZCI6Ik1GWXdUcVJNTUVveERUQUxCZ05WQkFvTUJFOVBWRk14SWpBZ0JnTlZCQXNNR1VaUFVpQlVSVk5VU1U1SElGQlZVbEJQVTBWVElFOU9URmt4RlRBVEJnTlZCQU1NREc5dmRITXRZM010ZEdWemRBSUVaVnhnRXc9PSIsIng1dCNTMjU2IjoiYU5XLWlxZW1XbDZYbnlZcVRFaFFuOHBGQ3ZTeDhTSHF1VVg2Mndxb0QwVSIsIng1YyI6WyJNSUlCaFRDQ0FTcWdBd0lCQWdJRVpWeGdFekFLQmdncWhrak9QUVFEQWpCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdIaGNOTWpNeE1USXhNRGMwTlRJeldoY05NalF4TVRJd01EYzBOVEl6V2pCS01RMHdDd1lEVlFRS0RBUlBUMVJUTVNJd0lBWURWUVFMREJsR1QxSWdWRVZUVkVsT1J5QlFWVkpRVDFORlV5QlBUa3haTVJVd0V3WURWUVFEREF4dmIzUnpMV056TFhSbGMzUXdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBUmE4TTJtVnA0a0VPc2d0SVZiZ2tleXc1ZDdCeFFvOG9pdDkwalJRZXhLM2VFYWtOTFhCVUI1S2ljZ0xqZk02ejlRZGZFeStpc3RxQlNzeXZoa0RuNmZNQW9HQ0NxR1NNNDlCQU1DQTBrQU1FWUNJUUMyS2NIR3UyNUNwM1VjVmREUGhDK213MGVXZXZ4SFV6NWNZYmxKaDBnUXdBSWhBT2Z6S24zeEhCSDhRNzVSZVZPMTUxdkRvdFRaQW5adkFVb1NSaWJoeWVTVyJdLCJ0eXAiOiJqb3NlIiwiYjY0IjpmYWxzZSwic2lnVCI6IjIwMjMtMTEtMjFUMTA6NTE6NTlaIiwic2lnRCI6eyJtSWQiOiJodHRwOi8vdXJpLmV0c2kub3JnLzE5MTgyL0h0dHBIZWFkZXJzIiwicGFycyI6WyJkaWdlc3QiXX0sImNyaXQiOlsiYjY0Iiwic2lnVCIsInNpZ0QiXX0..6lEx2i8ZWk6Z7uRYQF6IseNfeMOxrr6m9Gk1nPrSOZGH5GnCV0SblhM-bJ3hNkqYBGSqZVaW5tcOy5gyTqv8uw";
        DSSDocument document = new InMemoryDocument(signature.getBytes());
        String digest = "SHA-256=9NVf+lva6i6WEQZ2UZRZrUvchRM4A/hi6p6jrwrqJv8=";
        HTTPHeader httpHeader = new HTTPHeader("digest", digest);
        JWSCompactDocumentValidator documentValidator = new JWSCompactDocumentValidator(document);
        CommonCertificateVerifier commonCertificateVerifier = new CommonCertificateVerifier();
        documentValidator.setCertificateVerifier(commonCertificateVerifier);
        documentValidator.setTokenExtractionStrategy(TokenExtractionStrategy.EXTRACT_CERTIFICATES_AND_TIMESTAMPS);

        ClassLoader classLoader = getClass().getClassLoader();
        assert certificateProperties != null;
        File file = new File(Objects.requireNonNull(classLoader.getResource(certificateProperties.getFilename())).getFile());

        CertificateSource signingCertificateSource = new KeyStoreCertificateSource(file,"PKCS12",
                certificateProperties.getPassword().toCharArray());

        try (Pkcs12SignatureToken token = new Pkcs12SignatureToken(file.getAbsolutePath(),
                new KeyStore.PasswordProtection(certificateProperties.getPassword().toCharArray()))) {


            DSSPrivateKeyEntry privateKey = null;
            List<DSSPrivateKeyEntry> keys = token.getKeys();
            for (DSSPrivateKeyEntry entry : keys) {
                privateKey = entry;
            }

            if (privateKey != null) {
                documentValidator.setSigningCertificateSource(signingCertificateSource);
                documentValidator.setDetachedContents(Arrays.asList(document, httpHeader));
                documentValidator.setProcessExecutor(new DefaultSignatureProcessExecutor());
                documentValidator.setSignaturePolicyProvider(new SignaturePolicyProvider());
                documentValidator.setValidationLevel(ValidationLevel.BASIC_SIGNATURES);
                documentValidator.setEnableEtsiValidationReport(true);
                documentValidator.setTokenIdentifierProvider(new UserFriendlyIdentifierProvider());
                documentValidator.setIncludeSemantics(true);

                CertificateVerifier cerVer = new CommonCertificateVerifier();
                CommonTrustedCertificateSource trustedCertificateSource = new CommonTrustedCertificateSource();
                trustedCertificateSource.importAsTrusted(signingCertificateSource);
                cerVer.setTrustedCertSources(trustedCertificateSource);
                documentValidator.setCertificateVerifier(cerVer);

                Reports reports = documentValidator.validateDocument();
                List<XmlSemantic> semantics = reports.getDetailedReport().getJAXBModel().getSemantic();
                for ( Message msg:  reports.getSimpleReport().getAdESValidationErrors(reports.getSimpleReport().getFirstSignatureId())) {
                    System.out.println(msg.getKey()+" "+msg.getValue());
                }
                System.out.println("--------- --------");
                for ( Message msg:  reports.getSimpleReport().getAdESValidationInfo(reports.getSimpleReport().getFirstSignatureId())) {
                    System.out.println(msg.getKey()+" "+msg.getValue());
                }
                for(XmlSemantic semantic : semantics){
                    System.out.println(semantic.getKey()+" "+semantic.getValue());
                    if(semantic.getKey().equals("PASSED")){
                        assertEquals("PASSED", semantic.getKey());
                    }
                }
            } else
            {
                System.out.println("Key not found");
            }
        }
    }

}
